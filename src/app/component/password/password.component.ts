import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { EmailValidator, NgForm } from '@angular/forms';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css'],
})
export class PasswordComponent implements OnInit {
  backendurl = 'http://localhost:3000/admin/1';
  successStatus: any;
  email = 'admin@gmail.com';
  constructor(private http: HttpClient) {}

  ngOnInit(): void {}
  onReset(
    postData: {
      id: 1;
      password: string;
      email: string;
    },
    form: NgForm
  ) {
    let id = 1;

    this.http.put(this.backendurl, postData).subscribe((responseData) => {
      console.log(responseData);
      this.successStatus = 'Password Updated successfullly';
    });
  }
}
