import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Employee } from '../employee';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  fetchedEmployees: Employee[] = [];
  backendurl = 'http://localhost:3000/employees';
  constructor(private http: HttpClient, private router: Router) {}
  isLoading = true;
  ngOnInit(): void {
    if (localStorage.getItem('isLoggedIn') == 'true') {
      this.fetchEmployees();
    } else {
      this.router.navigate(['login']);
    }
  }

  onCreateEmployee(
    postData: { first_name: string; last_name: string; email: string },
    form: NgForm
  ) {
    this.http.post(this.backendurl, postData).subscribe((responseData) => {
      console.log(responseData);
      this.fetchEmployees();
      form.reset();
    });
  }
  onFetchEmployees() {
    this.fetchEmployees();
  }

  onDeleteEmployee(id: number) {
    this.http.delete(this.backendurl + '/' + id).subscribe((response) => {
      console.log('Employee deleted: ' + response);

      this.fetchEmployees();
    });
  }

  fetchEmployees() {
    this.http
      .get(this.backendurl)
      .pipe(
        map((responseData) => {
          const EmployeeArray: Employee[] = [];
          for (const key in responseData) {
            var x = { ...(responseData as any)[key] };
            EmployeeArray.push(x);
          }
          this.isLoading = false;
          return EmployeeArray;
        })
      )
      .subscribe((employee) => {
        this.fetchedEmployees = employee;
        console.log(employee);
      });
  }
}
