import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../employee';
import { map } from 'rxjs/operators';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css'],
})
export class UpdateComponent implements OnInit {
  fetchedEmployees: Employee[] = [];
  backendurl = 'http://localhost:3000/employees';
  empById: any;
  successStatus: string | undefined;
  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (localStorage.getItem('isLoggedIn') == 'true') {
      this.getEmpById();
    } else {
      this.router.navigate(['login']);
    }
  }
  getEmpById(): void {
    let id = this.activatedRoute.snapshot.params['empId'];
    this.http.get<Employee>(this.backendurl + '/' + id).subscribe((data) => {
      this.empById = data;
    });
  }
  onUpdateEmployee(
    postData: {
      id: number;
      first_name: string;
      last_name: string;
      email: string;
    },
    form: NgForm
  ) {
    let id = this.activatedRoute.snapshot.params['empId'];

    this.http
      .put(this.backendurl + '/' + id, postData)
      .subscribe((responseData) => {
        console.log(responseData);
        this.successStatus = 'Employee Updated successfullly';
      });
  }
}
