import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(private http: HttpClient, private router: Router) {}

  ngOnInit(): void {}
  onLogin(
    postData: {
      password: string;
      email: string;
    },
    form: NgForm
  ) {
    this.http.get<any>('http://localhost:3000/admin').subscribe(
      (res) => {
        const user = res.find((a: any) => {
          return a.email === postData.email && a.password === postData.password;
        });
        if (user) {
          alert('Login Succesful');
          form.reset();
          localStorage.setItem('isLoggedIn', 'true');
          this.router.navigate(['home']);
        } else {
          alert('Invalid credentials');
        }
      },
      (err) => {
        alert('Something went wrong');
      }
    );
  }
}
