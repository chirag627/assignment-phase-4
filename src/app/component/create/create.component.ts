import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Employee } from '../employee';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {
  fetchedEmployees: Employee[] = [];
  backendurl = 'http://localhost:3000/employees';
  successStatus: string | undefined;

  constructor(private router: Router, private http: HttpClient) {}

  ngOnInit(): void {
    if (localStorage.getItem('isLoggedIn') != 'true') {
      this.router.navigate(['login']);
    }
  }

  onCreateEmployee(
    postData: { first_name: string; last_name: string; email: string },
    form: NgForm
  ) {
    this.http.post(this.backendurl, postData).subscribe((responseData) => {
      console.log(responseData);
      this.successStatus = 'Employee created successfully';
      alert('Employe added successfully');
      this.router.navigate(['home']);
    });
  }
}
